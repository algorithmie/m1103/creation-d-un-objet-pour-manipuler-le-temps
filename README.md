# Manipulation d'un objet pour créer et manipuler des instants

---

## On considère qu’on souhaite représenter un Instant par :
- l’heure,
- la minute et
- la seconde actuelles

> Dans cette fiche on considère que les différents Instant ont lieu dans la même journée.


### Question A
Écrire en Python la classe Instant.


### Question B
Écrire la fonction ```read_Instant()->Instant ``` qui après avoir demandé à l’utilisateur la valeur des heures, minutes et secondes renvoie un Instant initialisé avec les valeurs saisies.
 

### Question C
Écrire la fonction ```print_Instant(t :Instant) ``` qui admet en paramètre un instant et qui affiche ses valeurs de la forme h :m :s.


### Question D
Écrire la fonction ```estInstantValide(t :Instant)->bool: ``` qui retourne vrai si l’instant passé en paramètre est valide et faux sinon.


### Question E
Écrire la fonction ```suivant(t :Instant )->Instant: ``` qui admet en paramètre un Instant et qui retourne un Instant correspondant à l’instant passé en paramètre + 1 seconde.


### Question F
Écrire la fonction ```estPlusRecent(t1 :Instant,t2 :Instant)->bool: ``` qui admet deux instants en paramètres et qui retourne vrai si t1 est plus récent que t2 et faux sinon.


### Question G
Écrire la fonction ```duree(debut :Instant ,fin :Instant )->Instant: ``` qui admet en paramètres deux instant et qui retourne un Instant correspondant à la durée entre fin et debut.