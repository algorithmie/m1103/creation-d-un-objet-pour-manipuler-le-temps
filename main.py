class Instant():
    def __init__(self, h=00, m=00, s=00):
        self.heure = h
        self.minute = m
        self.seconde = s


def read_Instant(i: Instant) -> Instant:
    i.heure = int(input("Saisir les heures: "))
    i.minute = int(input("Saisir les minutes: "))
    i.seconde = int(input("Sasir les secondes: "))
    if estInstantValide(i) == 0:
        print("Format incorrect !")
        read_Instant(i)


def print_Instant(t: Instant):
    print(t.heure, ":", t.minute, ":", t.seconde)


def estInstantValide(t: Instant) -> bool:
    if t.heure < 24 and t.heure > 0:
        if t.minute < 60 and t.minute > 0:
            if t.seconde < 60 and t.seconde > 0:
                return True
            else:
                return False
        else:
            return False
    else:
        return False


def suivant(t: Instant) -> Instant:
    t.seconde += 1

    if t.seconde == 60:
        t.minute += 1
        t.seconde = 00

    if t.minute == 60:
        t.heure += 1
        t.minute = 00

    if t.heure > 23:
        return False


# Tester /!\
def estPlusRecent(t1: Instant, t2: Instant) -> bool:
    if t1.heure < t2.heure:
        if t1.minute < t2.minute:
            if t1.seconde < t1.seconde:
                return True
            else:
                return False
        else:
            return False
    else:
        return False


def duree(debut: Instant, fin: Instant) -> Instant:
    resultat = Instant()
    resultat.heure = fin.heure - debut.heure
    resultat.minute = fin.minute - debut.minute
    resultat.seconde = fin.seconde - debut.seconde
    return print_Instant(resultat)

if __name__ == "__main__":
    instant = Instant()
    instant2 = Instant()
    read_Instant(instant)
    read_Instant(instant2)
    print_Instant(instant)
    print_Instant(instant2)
    print(estInstantValide(instant))
    suivant(instant)
    suivant(instant)
    suivant(instant)

    suivant(instant2)
    suivant(instant2)
    suivant(instant2)
    suivant(instant2)
    suivant(instant2)
    suivant(instant2)

    print_Instant(instant)
    print_Instant(instant2)

    print(estPlusRecent(instant, instant2))

    print("Durée: ")
    print(duree(instant, instant2))


# Certaines erreurs sont à prévoir dans la fonction |  duree(debut: Instant, fin: Instant) -> Instant:  |